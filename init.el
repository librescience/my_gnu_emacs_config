;; melpa stable repository
(require 'package)
(add-to-list 'package-archives
             '("MELPA Stable" . "http://stable.melpa.org/packages/") t)
(package-initialize)
;;(package-refresh-contents)

;; disable menu bar
(menu-bar-mode -1)

;; disable tool bar
(tool-bar-mode -1)

;; disable scroll bar
(scroll-bar-mode -1)

;; disable auto-save-list
(setq auto-save-list-file-prefix nil)

;; disable ~ files
(setq make-backup-files nil)

;; disable .# files
(setq create-lockfiles nil)

;; stop creating those #auto-save# files
(setq auto-save-default nil)

;; disable arrow keys
(global-unset-key (kbd "<left>"))
(global-unset-key (kbd "<right>"))
(global-unset-key (kbd "<up>"))
(global-unset-key (kbd "<down>"))
(global-unset-key (kbd "<C-left>"))
(global-unset-key (kbd "<C-right>"))
(global-unset-key (kbd "<C-up>"))
(global-unset-key (kbd "<C-down>"))
(global-unset-key (kbd "<M-left>"))
(global-unset-key (kbd "<M-right>"))
(global-unset-key (kbd "<M-up>"))
(global-unset-key (kbd "<M-down>"))

;; change font face and font size
(set-frame-font "Hack 14" nil t)

;; line spacing
(setq-default line-spacing 0.2)

;; stop cursor blink
(blink-cursor-mode 0)

;; change cursor type
(setq-default cursor-type 'bar)

;; show line numbers
(global-linum-mode 1)

;; padding line numbers
(setq linum-format " %d ")

;; UTF-8 as default file encoding
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

;; open init.el quickly
(global-set-key (kbd "<f12>") (lambda () (interactive) (find-file "~/.emacs.d/init.el")))

;; resize windows
(global-set-key (kbd "<s-up>") 'shrink-window)
(global-set-key (kbd "<s-down>") 'enlarge-window)
(global-set-key (kbd "<s-left>") 'shrink-window-horizontally)
(global-set-key (kbd "<s-right>") 'enlarge-window-horizontally)

;; auto-close brackets
(electric-pair-mode 1)
;; make electric-pair-mode work on more brackets
(setq electric-pair-pairs
      '(
        (?\" . ?\")
        (?\{ . ?\})))

;; disable mouse
(dolist (k '([mouse-1] [down-mouse-1] [drag-mouse-1] [double-mouse-1] [triple-mouse-1]
             [mouse-2] [down-mouse-2] [drag-mouse-2] [double-mouse-2] [triple-mouse-2]
             [mouse-3] [down-mouse-3] [drag-mouse-3] [double-mouse-3] [triple-mouse-3]
             [mouse-4] [down-mouse-4] [drag-mouse-4] [double-mouse-4] [triple-mouse-4]
             [mouse-5] [down-mouse-5] [drag-mouse-5] [double-mouse-5] [triple-mouse-5]))
  (global-unset-key k))

;; duplicate lines
(defun duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
)
(global-set-key (kbd "C-d") 'duplicate-line)

;; display column numbers
(setq column-number-mode t)

;; highlight current line
(global-hl-line-mode 1)
(set-face-background 'highlight "#eee")
(set-face-foreground 'highlight nil)

;; turn on highlight matching brackets
(show-paren-mode 1)
;; highlight brackets
(setq show-paren-style 'parenthesis)
;; highlight entire expression
(setq show-paren-style 'expression)
;; highlight brackets if visible, else entire expression
(setq show-paren-style 'mixed)
;; change brackets highlight color
(require 'paren)
(set-face-background 'show-paren-match (face-background 'default))
(set-face-foreground 'show-paren-match "purple")
(set-face-attribute 'show-paren-match nil :weight 'extra-bold)

;; fill-column-indicator
(add-to-list 'load-path "~/.emacs.d/lisp/fill-column-indicator/")
(require 'fill-column-indicator)
(setq fci-rule-column 79)
(setq fci-rule-width 1)
(setq fci-rule-color "purple")
(define-globalized-minor-mode global-fci-mode fci-mode (lambda () (fci-mode 1)))
(global-fci-mode 1)
(define-globalized-minor-mode global-fci-mode fci-mode
  (lambda ()
    (if (and
         (not (string-match "^\*.*\*$" (buffer-name)))
         (not (eq major-mode 'dired-mode)))
        (fci-mode 1))))
(global-fci-mode 1)

;; Advance Tab
;; Create a variable for our preferred tab width
(setq custom-tab-width 4)

;; Two callable functions for enabling/disabling tabs in Emacs
(defun disable-tabs () (setq indent-tabs-mode nil))
(defun enable-tabs  ()
  (local-set-key (kbd "TAB") 'tab-to-tab-stop)
  (setq indent-tabs-mode t)
  (setq tab-width custom-tab-width))

;; Hooks to Enable Tabs
(add-hook 'prog-mode-hook 'enable-tabs)
;; Hooks to Disable Tabs
(add-hook 'lisp-mode-hook 'disable-tabs)
(add-hook 'emacs-lisp-mode-hook 'disable-tabs)

;; Making electric-indent behave sanely
(setq-default electric-indent-inhibit t)

;; Make the backspace properly erase the tab instead of
;; removing 1 space at a time.
(setq backward-delete-char-untabify-method 'hungry)

;; disable whitespace-mode color
(setq whitespace-style (quote (face tabs newline space-mark tab-mark newline-mark)))

;; move lines up and down
(defun move-line (n)
  "Move the current line up or down by N lines."
  (interactive "p")
  (setq col (current-column))
  (beginning-of-line) (setq start (point))
  (end-of-line) (forward-char) (setq end (point))
  (let ((line-text (delete-and-extract-region start end)))
    (forward-line n)
    (insert line-text)
    ;; restore point to original column in moved line
    (forward-line -1)
    (forward-char col)))

(defun move-line-up (n)
  "Move the current line up by N lines."
  (interactive "p")
  (move-line (if (null n) -1 (- n))))

(defun move-line-down (n)
  "Move the current line down by N lines."
  (interactive "p")
  (move-line (if (null n) 1 n)))

(global-set-key (kbd "M-<up>") 'move-line-up)
(global-set-key (kbd "M-<down>") 'move-line-down)

;; change selected text
(set-face-attribute 'region nil :background "#ccc")

;; display time
(display-time)

;; auto-refresh all buffers
(global-auto-revert-mode t)

;; disable bold text
(set-face-bold-p 'bold nil)

;; disable underline text
 (mapc
  (lambda (face)
    (set-face-attribute face nil :weight 'normal :underline nil))
  (face-list))

;; splash screen
(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)

;; change splash screen text
      (insert " " )  
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(custom-enabled-themes '(deeper-blue))
 '(display-time-mode t)
 '(menu-bar-mode nil)
 '(package-selected-packages
   '(ac-ispell projectile auto-complete py-autopep8 company-jedi elpy helm magit company avy dash chess inf-ruby emmet-mode))
 '(send-mail-function 'smtpmail-send-it)
 '(show-paren-mode t)
 '(tab-bar-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-scrollbar-bg ((t (:background "#ffffffffffff"))))
 '(company-scrollbar-fg ((t (:background "#ffffffffffff"))))
 '(company-tooltip ((t (:background "#222" :foreground "#fff"))))
 '(company-tooltip-common ((t (:inherit font-lock-constant-face))))
 '(company-tooltip-selection ((t (:inherit font-lock-function-name-face)))))

;; show/hide dot-files in dired
  (defun dired-dotfiles-toggle ()    
    (interactive)
    (when (equal major-mode 'dired-mode)
      (if (or (not (boundp 'dired-dotfiles-show-p)) dired-dotfiles-show-p) ; if currently showing
	  (progn 
	    (set (make-local-variable 'dired-dotfiles-show-p) nil)
	    (message "h")
	    (dired-mark-files-regexp "^\\\.")
	    (dired-do-kill-lines))
	(progn (revert-buffer) ; otherwise just revert to re-show
	       (set (make-local-variable 'dired-dotfiles-show-p) t)))))
(global-set-key (kbd "C-c h") 'dired-dotfiles-toggle)

;; toggle comment/uncommentn
(defun toggle-comment-dwim ()
  "Like `comment-dwim', but toggle comment if cursor is not at end of line."

  (interactive)
  (if (region-active-p)
      (comment-dwim nil)
    (let (($lbp (line-beginning-position))
          ($lep (line-end-position)))
      (if (eq $lbp $lep)
          (progn
            (comment-dwim nil))
        (if (eq (point) $lep)
            (progn
              (comment-dwim nil))
          (progn
            (comment-or-uncomment-region $lbp $lep)
            (forward-line )))))))
(global-set-key (kbd "C-c c") 'toggle-comment-dwim)

;; C
(setq c-default-style "linux"
          c-basic-offset 4)

;; Python
;; python3 as default
(setq python-shell-interpreter "python3.9")
;; run python code: (C-c)
(global-set-key (kbd "<f8>") 'run-python)
;; disable showing warning message in python-mode
(setq python-indent-guess-indent-offset nil)  
(setq python-indent-guess-indent-offset-verbose nil)
;; python tab size
(setq python-indent-offset 4)

;; Ruby
;; run ruby code: press f9 and select all code then (C-c C-r)
;; ruby tab size
(setq ruby-indent-level 2)
;; inf-ruby
(add-to-list 'load-path "~/.emacs.d/lisp/inf-ruby/")
(require 'inf-ruby)
(autoload 'inf-ruby-minor-mode "inf-ruby" "Run an inferior Ruby process" t)
(add-hook 'ruby-mode-hook 'inf-ruby-minor-mode)
(global-set-key (kbd "<f9>") 'inf-ruby)

;; CSS
;; css tab size
(setq css-indent-offset 2)

;; JavaScript
;; javascript tab size
(setq js-indent-level 2)

;; emmet-mode (press C-j)
(add-to-list 'load-path "~/.emacs.d/lisp/emmet-mode/")
(require 'emmet-mode)
(add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
(add-hook 'css-mode-hook  'emmet-mode) ;; enable Emmet's css abbreviation.
(setq emmet-move-cursor-between-quotes t) ;; default nil
(setq emmet-self-closing-tag-style "") ;; default "/"

;; SQL
;; Automatically upcase SQL keywords
  (defun upcase-sql-keywords ()
    (interactive)
    (save-excursion
      (dolist (keywords sql-mode-postgres-font-lock-keywords)
        (goto-char (point-min))
        (while (re-search-forward (car keywords) nil t)
          (goto-char (+ 1 (match-beginning 0)))
          (when (eql font-lock-keyword-face (face-at-point))
            (backward-char)
            (upcase-word 1)
            (forward-char))))))
(global-set-key (kbd "C-.") 'upcase-sql-keywords)

;; MySQL
;; run mysql code: open a .sql file then press M-x sql-mysql then press (C-c C-b) to run sql
;; Enter mysql command line: open your shell then: mysql -u USERNAME -p
;; postgresql (sql-postgres - login with root user)
;; sqlite = M-x sql-sqlite  then open .sqlite file and done!
(defun point-in-comment ()
  (let ((syn (syntax-ppss)))
    (and (nth 8 syn)
         (not (nth 3 syn)))))
(defun my-capitalize-all-mysql-keywords ()
  (interactive)
  (require 'sql)
  (save-excursion
    (dolist (keywords sql-mode-mysql-font-lock-keywords) 
      (goto-char (point-min))
      (while (re-search-forward (car keywords) nil t)
        (unless (point-in-comment)
          (goto-char (match-beginning 0))
          (upcase-word 1))))))
(global-set-key (kbd "C-,") 'my-capitalize-all-mysql-keywords)

;; org-mode
(setq org-startup-folded nil)

;; Alias for fast M-x
(defalias 'yes-or-no-p 'y-or-n-p)
(defalias 'color 'list-colors-display)
(defalias 'hs 'hs-minor-mode) ;; enable hide/show
(defalias 'hide 'hs-hide-all)
(defalias 'show 'hs-show-all)
(defalias 'theme 'customize-themes)
(defalias 'uptime 'emacs-uptime)
(defalias 'maria 'sql-mariadb)
(defalias 'postgre 'sql-postgres)
(defalias 'sqlite 'sql-sqlite)
(defalias 'replace 'query-replace)
(defalias 'refresh 'eval-buffer)
(defalias 'ut 'undo-tree-visualize)
(defalias 'package 'list-packages)
;;(defalias 'flycheck 'global-flycheck-mode)

;; multiple cursors
(add-to-list 'load-path "~/.emacs.d/lisp/multiple-cursors/")
(require 'multiple-cursors)
(global-set-key (kbd "C-c m c") 'mc/edit-lines)

;; tab-bar
(global-set-key (kbd "C-x w") 'tab-close)
(global-set-key (kbd "C-x t") 'tab-new)
(global-set-key (kbd "C-<right>") 'tab-next)
(global-set-key (kbd "C-<left>") 'tab-previous)

;; easy upper/lower case characters
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;; default theme
(load-theme 'deeper-blue)

;; load default theme
(defun disable-all-themes ()
  "disable all active themes."
  (dolist (i custom-enabled-themes)
    (disable-theme i)))

(defadvice load-theme (before disable-themes-first activate)
  (disable-all-themes))
(global-set-key (kbd "C-<f12>") 'load-theme) ;; then press enter

;; Delete all lines
(defun kill-to-end-of-buffer()
  (interactive)
  (progn
    (forward-line -999999)
    (delete-region (point) (point-max))))
(global-set-key "\C-\M-k" 'kill-to-end-of-buffer)

;; change all/some query to your text
(global-set-key (kbd "C-c r") 'query-replace)

;; emacs file manager [dired] sort (by date, size and name)
(defun dired-sort ()
  (interactive)
  (let ($sort-by $arg)
    (setq $sort-by (ido-completing-read "Sort by:" '( "date" "size" "name" )))
    (cond
     ((equal $sort-by "name") (setq $arg "-Al "))
     ((equal $sort-by "date") (setq $arg "-Al -t"))
     ((equal $sort-by "size") (setq $arg "-Al -S"))
     ;; ((equal $sort-by "dir") (setq $arg "-Al --group-directories-first"))
     (t (error "logic error 09535" )))
    (dired-sort-other $arg )))
(require 'dired )
(define-key dired-mode-map (kbd "s") 'dired-sort)

;; everytime bookmark is changed, automatically save it
(setq bookmark-save-flag 1)

;; windmove
(global-set-key (kbd "<left>")  'windmove-left)
(global-set-key (kbd "<right>") 'windmove-right)
(global-set-key (kbd "<up>")    'windmove-up)
(global-set-key (kbd "<down>")  'windmove-down)
(defun ignore-error-wrapper (fn)
  "Funtion return new function that ignore errors.
   The function wraps a function with `ignore-errors' macro."
  (lexical-let ((fn fn))
    (lambda ()
      (interactive)
      (ignore-errors
        (funcall fn)))))

;; undo-tree
(add-to-list 'load-path "~/.emacs.d/lisp/undo-tree/")
(require 'undo-tree)
(defun undo-tree-visualizer-update-linum (&rest args)
    (linum-update undo-tree-visualizer-parent-buffer))
(advice-add 'undo-tree-visualize-undo :after #'undo-tree-visualizer-update-linum)
(advice-add 'undo-tree-visualize-redo :after #'undo-tree-visualizer-update-linum)
(advice-add 'undo-tree-visualize-undo-to-x :after #'undo-tree-visualizer-update-linum)
(advice-add 'undo-tree-visualize-redo-to-x :after #'undo-tree-visualizer-update-linum)
(advice-add 'undo-tree-visualizer-mouse-set :after #'undo-tree-visualizer-update-linum)
(advice-add 'undo-tree-visualizer-set :after #'undo-tree-visualizer-update-linum)

;; avy (quick jump to your word)
(global-set-key (kbd "C-;") 'avy-goto-char)
(global-set-key (kbd "C-'") 'avy-goto-char-2)
(global-set-key (kbd "M-g f") 'avy-goto-line)
(global-set-key (kbd "M-g w") 'avy-goto-word-1)
(global-set-key (kbd "M-g e") 'avy-goto-word-0)
(avy-setup-default)
(global-set-key (kbd "C-c C-j") 'avy-resume)

;; clear shell
(global-set-key (kbd "C-c l") 'comint-clear-buffer)

;; company-mode
;; enable by default
(add-hook 'after-init-hook 'global-company-mode)
;; No delay in showing suggestions.
(setq company-idle-delay 0)
;; Show suggestions after entering one character.
(setq company-minimum-prefix-length 1)
; Use tab key to cycle through suggestions.
;; change color
  (require 'color)
  
  (let ((bg (face-attribute 'default :background)))
    (custom-set-faces
     '(company-tooltip ((t (:background "#222" :foreground "#fff"))))
     `(company-scrollbar-bg ((t (:background ,(color-lighten-name bg 10)))))
     `(company-scrollbar-fg ((t (:background ,(color-lighten-name bg 5)))))
     `(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
     `(company-tooltip-common ((t (:inherit font-lock-constant-face))))))
;; correct css
  (add-hook 'css-mode-hook
            (lambda ()
              (set (make-local-variable 'company-backends) '(company-css))))
;; complete all words in current page
  (add-hook 'completion-at-point-functions 'pcomplete-completions-at-point nil t)

;; helm
(require 'helm-config)
(helm-mode 1)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-c C-f") #'helm-find-files)

;; elpy
;;(elpy-enable)
;; Solving company, yasnippet conflicts
(defun company-yasnippet-or-completion ()
  "Solve company yasnippet conflicts."
  (interactive)
  (let ((yas-fallback-behavior
         (apply 'company-complete-common nil)))
    (yas-expand)))

(add-hook 'company-mode-hook
          (lambda ()
            (substitute-key-definition
             'company-complete-common
             'company-yasnippet-or-completion
             company-active-map)))

;; flycheck
;; (add-hook 'after-init-hook #'global-flycheck-mode)

;; auto-complete
;;(ac-config-default)
;; (global-auto-complete-mode t)

;; dictionary for company-mode
(global-set-key (kbd "M-/") 'hippie-expand)

;; The actual expansion function
(defun try-expand-by-dict (old)
  ;; old is true if we have already attempted an expansion
  (unless (bound-and-true-p ispell-minor-mode)
    (ispell-minor-mode 1))

  ;; english-words.txt is the fallback dicitonary
  (if (not ispell-alternate-dictionary)
      (setq ispell-alternate-dictionary (file-truename "~/.emacs.d/misc/english-words.txt")))
  (let ((lookup-func (if (fboundp 'ispell-lookup-words)
                       'ispell-lookup-words
                       'lookup-words)))
    (unless old
      (he-init-string (he-lisp-symbol-beg) (point))
      (if (not (he-string-member he-search-string he-tried-table))
        (setq he-tried-table (cons he-search-string he-tried-table)))
      (setq he-expand-list
            (and (not (equal he-search-string ""))
                 (funcall lookup-func (concat (buffer-substring-no-properties (he-lisp-symbol-beg) (point)) "*")))))
    (if (null he-expand-list)
      (if old (he-reset-string))
      (he-substitute-string (car he-expand-list))
      (setq he-expand-list (cdr he-expand-list))
      t)
    ))

(setq hippie-expand-try-functions-list
      '(;; try-expand-dabbrev
        ;; try-expand-dabbrev-all-buffers
        try-expand-by-dict))
